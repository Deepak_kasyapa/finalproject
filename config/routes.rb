Rails.application.routes.draw do
  get 'ps6/six'
  get 'ps3/paticipant'
  get 'ps3/student'
  get 'ps3/index'
  get 'ps3/staff'
  get 'ps5/five'
  get 'ps4/four'
  get 'ps2/two'
  get 'ps1/one'
  get 'basics/solutions'
  get 'support/indexc'
  get 'election/indexb'




  post 'comment/store'
  get "store", to: "comment#store"
  devise_for :users

  
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)  
  
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
 

  get "home", to: "home#index"
  # Defines the root path route ("/")
  # root "articles#index"
  root 'basics#solutions'
  
end
