Given(/^I am a user$/) do
    @user = FactoryBot.create :owner
  end

Given(/^I am signed in$/) do
    visit '/users/sign_in'
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'Log in'
end

Given(/^I see manage website$/) do
    click_link 'Manage website'
end

When(/^I visit the comment section$/) do
    visit '/comment'
end


