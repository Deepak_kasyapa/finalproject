require "test_helper"

class BasicsControllerTest < ActionDispatch::IntegrationTest
  test "should get solutions" do
    get basics_solutions_url
    assert_response :success
  end
end
