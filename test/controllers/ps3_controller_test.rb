require "test_helper"

class Ps3ControllerTest < ActionDispatch::IntegrationTest
  test "should get paticipant" do
    get ps3_paticipant_url
    assert_response :success
  end

  test "should get student" do
    get ps3_student_url
    assert_response :success
  end

  test "should get staff" do
    get ps3_staff_url
    assert_response :success
  end
end
